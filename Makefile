CC=gcc
PREFIX=/usr/local

random: random.c
	$(CC) -o random -Wall -Wextra random.c

install: random
	cp random $(PREFIX)/bin
