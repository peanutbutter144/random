# random

_generate one pseudo-random positive integer_

The maximum and minimum values (inclusive) are provided by the user as command-line arguments.


## Dependencies

* git
* make
* gcc

## Installation

    git clone https://gitlab.com/peanutbutter144/random.git
    cd random
    make
    sudo make install

The executable will be created and you can install it yourself.
